# Auto DevOps
# This CI/CD configuration provides a standard pipeline for
# * building a Docker image (using a buildpack if necessary),
# * storing the image in the container registry,
# * running tests from a buildpack,
# * running code quality analysis,
# * creating a review app for each topic branch,
# * and continuous deployment to production
#
# Test jobs may be disabled by setting environment variables:
# * test: TEST_DISABLED
# * code_quality: CODE_QUALITY_DISABLED
# * license_management: LICENSE_MANAGEMENT_DISABLED
# * performance: PERFORMANCE_DISABLED
# * load_performance: LOAD_PERFORMANCE_DISABLED
# * sast: SAST_DISABLED
# * secret_detection: SECRET_DETECTION_DISABLED
# * dependency_scanning: DEPENDENCY_SCANNING_DISABLED
# * container_scanning: CONTAINER_SCANNING_DISABLED
# * dast: DAST_DISABLED
# * review: REVIEW_DISABLED
# * stop_review: REVIEW_DISABLED
#
# In order to deploy, you must have a Kubernetes cluster configured either
# via a project integration, or via group/project variables.
# KUBE_INGRESS_BASE_DOMAIN must also be set on the cluster settings,
# as a variable at the group or project level, or manually added below.
#
# Continuous deployment to production is enabled by default.
# If you want to deploy to staging first, set STAGING_ENABLED environment variable.
# If you want to enable incremental rollout, either manual or time based,
# set INCREMENTAL_ROLLOUT_MODE environment variable to "manual" or "timed".
# If you want to use canary deployments, set CANARY_ENABLED environment variable.
#
# If Auto DevOps fails to detect the proper buildpack, or if you want to
# specify a custom buildpack, set a project variable `BUILDPACK_URL` to the
# repository URL of the buildpack.
# e.g. BUILDPACK_URL=https://github.com/heroku/heroku-buildpack-ruby.git#v142
# If you need multiple buildpacks, add a file to your project called
# `.buildpacks` that contains the URLs, one on each line, in order.
# Note: Auto CI does not work with multiple buildpacks yet

image: alpine:latest

variables:
  # KUBE_INGRESS_BASE_DOMAIN is the application deployment domain and should be set as a variable at the group or project level.
  # KUBE_INGRESS_BASE_DOMAIN: domain.example.com
  CI_AWS_S3_PUSH_FILE: "aws/s3_push.json"
  CI_AWS_EC2_DEPLOYMENT_FILE: "aws/create_deployment.json"

  POSTGRES_USER: user
  POSTGRES_PASSWORD: testing-password
  POSTGRES_ENABLED: "true"
  POSTGRES_DB: $CI_ENVIRONMENT_SLUG

  #  TEST_DISABLED: "true"
  CODE_QUALITY_DISABLED: "true"
  LICENSE_MANAGEMENT_DISABLED: "true"
  PERFORMANCE_DISABLED: "true"
  LOAD_PERFORMANCE_DISABLED: "true"
  SAST_DISABLED: "true"
  SECRET_DETECTION_DISABLED: "true"
  DEPENDENCY_SCANNING_DISABLED: "true"
  CONTAINER_SCANNING_DISABLED: "true"
  DAST_DISABLED: "true"
  REVIEW_DISABLED: "true"

  DOCKER_DRIVER: overlay2

  ROLLOUT_RESOURCE_TYPE: deployment

  DOCKER_TLS_CERTDIR: "" # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501

stages:
  - build
  - test
  - deploy # dummy stage to follow the template guidelines
  - review
  - dast
  - staging
  - canary
  - production
  - incremental rollout 10%
  - incremental rollout 25%
  - incremental rollout 50%
  - incremental rollout 100%
  - performance
  - documentation
  - cleanup

workflow:
  rules:
    - if: '$BUILDPACK_URL || $AUTO_DEVOPS_EXPLICITLY_ENABLED == "1" || $DOCKERFILE_PATH'

    - exists:
        - Dockerfile

    # https://github.com/heroku/heroku-buildpack-clojure
    - exists:
        - project.clj

    # https://github.com/heroku/heroku-buildpack-go
    - exists:
        - go.mod
        - Gopkg.mod
        - Godeps/Godeps.json
        - vendor/vendor.json
        - glide.yaml
        - src/**/*.go

    # https://github.com/heroku/heroku-buildpack-gradle
    - exists:
        - gradlew
        - build.gradle
        - settings.gradle

    # https://github.com/heroku/heroku-buildpack-java
    - exists:
        - pom.xml
        - pom.atom
        - pom.clj
        - pom.groovy
        - pom.rb
        - pom.scala
        - pom.yaml
        - pom.yml

    # https://github.com/heroku/heroku-buildpack-multi
    - exists:
        - .buildpacks

    # https://github.com/heroku/heroku-buildpack-nodejs
    - exists:
        - package.json

    # https://github.com/heroku/heroku-buildpack-php
    - exists:
        - composer.json
        - index.php

    # https://github.com/heroku/heroku-buildpack-play
    # TODO: detect script excludes some scala files
    - exists:
        - "**/conf/application.conf"

    # https://github.com/heroku/heroku-buildpack-python
    # TODO: detect script checks that all of these exist, not any
    - exists:
        - requirements.txt
        - setup.py
        - Pipfile

    # https://github.com/heroku/heroku-buildpack-ruby
    - exists:
        - Gemfile

    # https://github.com/heroku/heroku-buildpack-scala
    - exists:
        - "*.sbt"
        - project/*.scala
        - .sbt/*.scala
        - project/build.properties

    # https://github.com/dokku/buildpack-nginx
    - exists:
        - .static

# NOTE: These links point to the latest templates for development in GitLab canonical project,
# therefore the actual templates that were included for Auto DevOps pipelines
# could be different from the contents in the links.
# To view the actual templates, please replace `master` to the specific GitLab version when
# the Auto DevOps pipeline started running e.g. `v13.0.2-ee`.
include:
  - template: Jobs/Build.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml
#  - template: Jobs/Test.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Test.gitlab-ci.yml
  - template: Jobs/Code-Quality.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/Browser-Performance-Testing.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Browser-Performance-Testing.gitlab-ci.yml
  - template: Security/DAST.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Secret-Detection.gitlab-ci.yml
    
production_ec2:
  image: "registry.gitlab.com/gitlab-org/cloud-deploy/aws-ec2:latest"
  script:
    - sed -i "s/%TAG%/$CI_COMMIT_SHA/g" aws/*
    - cat aws/s3_push.json
    - gl-ec2 push-to-s3
    - gl-ec2 deploy-to-ec2
  stage: production
  environment:
    name: production
  rules:
    - if: '$AUTO_DEVOPS_PLATFORM_TARGET != "EC2"'
      when: never
    - if: "$CI_KUBERNETES_ACTIVE"
      when: never
    - if: '$CI_COMMIT_BRANCH != "panterasbox"'
      when: never
    - if: "$CI_COMMIT_TAG || $CI_COMMIT_BRANCH"

build_artifact:
  stage: build
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-build-image:v0.4.0"
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:19.03.12-dind
  script:
    - |
      if [[ -z "$CI_COMMIT_TAG" ]]; then
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
      else
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
      fi
    - /build/build.sh
  rules:
    - if: '$AUTO_DEVOPS_PLATFORM_TARGET != "EC2"'
      when: never
    - if: "$CI_COMMIT_TAG || $CI_COMMIT_BRANCH"
